import java.util.Properties
import org.gradle.api.JavaVersion
import com.google.firebase.appdistribution.gradle.firebaseAppDistribution
import com.google.firebase.crashlytics.buildtools.gradle.CrashlyticsExtension

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.gms.google-services")

    // Add the Crashlytics Gradle plugin
    id("com.google.firebase.crashlytics")

    // Add the App Distribution Gradle plugin
    id("com.google.firebase.appdistribution")


}

android {
    namespace = "com.jerry.gitlab_cicd"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.jerry.gitlab_cicd"
        minSdk = 24
        targetSdk = 34
        versionCode = 3
        versionName = "1.0.2"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }

        //https://www.giorgosneokleous.com/2019/12/01/name-your-apk-aab-files/
        setProperty("archivesBaseName", "your-new-name")
    }

    signingConfigs {
        create("release") {

            storeFile = file("${rootDir}/release.keystore")

            val releaseKeystorePropertiesFile = readProperties(file("${rootDir}/release.properties"))
            if (releaseKeystorePropertiesFile == null){
                println("not found ${"${rootDir}/release.properties"}")
            }
            storePassword = releaseKeystorePropertiesFile["storePassword"] as String
            keyAlias = releaseKeystorePropertiesFile["keyAlias"] as String
            keyPassword = releaseKeystorePropertiesFile["keyPassword"] as String
        }

        getByName("debug") {
            storeFile = file("${rootDir}/debug.keystore")
        }

    }

    buildTypes {

        release {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            //signingConfig = signingConfigs.getByName("release")

            firebaseAppDistribution {
                //appId="1:1234567890:android:321abc456def7890"
                serviceCredentialsFile = "$rootDir/app-distribution-key.json"
                artifactType = "APK"
                releaseNotesFile = "${rootDir}/release-notes.txt"
                testers="testjerry10@gmail.com"
            }

            //https://github.com/firebase/firebase-android-sdk/issues/4171
            configure<CrashlyticsExtension> {
                mappingFileUploadEnabled = true //isMinifyEnabled should SAME!!
            }
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }


}

fun readProperties(propertiesFile: File) = Properties().apply {
    propertiesFile.inputStream().use { fis ->
        load(fis)
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation("androidx.activity:activity-compose:1.8.1")
    implementation(platform("androidx.compose:compose-bom:2023.03.00"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation(platform("androidx.compose:compose-bom:2023.03.00"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")

    //firebase
    implementation(platform("com.google.firebase:firebase-bom:32.6.0"))
    implementation("com.google.firebase:firebase-crashlytics")
    implementation("com.google.firebase:firebase-analytics")
}